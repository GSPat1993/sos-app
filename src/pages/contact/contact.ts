import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import { Contacts } from '@ionic-native/Contacts';

import { ContactService } from './contact.service'

import * as _ from 'lodash';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  uid: any;
  users: any;
  loader: any;
  allUsers: any;
  selectedUsers: any;
  noContactsToShow: Boolean;

  constructor(
    public contact: Contacts,
    public navCtrl: NavController,
    public contactServ: ContactService,
    public loadingCtrl: LoadingController
  ) { }

  ionViewDidEnter() {
    let mapUsers: Array<any>;
    let phoneContacts: Array<any>;
    let existContacts: Array<any>;

    this.loader = this.loadingCtrl.create({
      content: 'loading contacts...'
    });

    this.loader.present();

    this.uid = sessionStorage.getItem('user_id');
    this.contactServ.getAllUser().then(allUser => {
      phoneContacts = [];
      existContacts = [];
      this.users = allUser.val();

      if (!this.users) {
        this.noContactsToShow = true;
        return;
      }

      this.contact.find(['phoneNumbers', 'displayName'], {filter: "", hasPhoneNumber: true, desiredFields: ['phoneNumbers']}).then(phoneBook => {
        console.log('Phonebook: ', phoneBook);
        
        _.forEach(phoneBook, (phoneValue) => {
          _.forEach(phoneValue.phoneNumbers, (valPhone) => {
            phoneContacts.push(valPhone.value.replace(/\s/g, '').replace('+63', '').replace('+63', '').replace(/^0+/, ''));
            _.forOwn(this.users, (userPhone, keyPhone) => {
              if (userPhone.phone_number === (valPhone.value).replace(/\s/g, '').replace('+63', '').replace(/^0+/, '') && keyPhone !== this.uid) {
                existContacts.push(keyPhone);
              }
            });
          });
        });

        console.log('PHONE CONTACTS: ', phoneContacts);
        console.log('EXIST CONTACTS: ', _.uniq(existContacts));

        this.contactServ.getContacts(this.uid).subscribe(selectedContacts => {

          // this.contact.pickContact().then(data => {
          //   console.log('PICKCONTACT: ', data);
          // });

          this.loader.dismiss();
          this.noContactsToShow = false;
          this.selectedUsers = selectedContacts;

          console.log('USERS: ', allUser.val());
          console.log('RESULT: ', selectedContacts);

          mapUsers = _.map(_.uniq(existContacts), (val, key) => {
            return {$key: val, active: this.users[val].active, date_registered: this.users[val].date_registered, email: this.users[val].email, firstname: this.users[val].firstname, lastname: this.users[val].lastname, phone_number: this.users[val].phone_number};
          });

          _.forEach(selectedContacts, (selectVal) => {
            _.remove(mapUsers, (val) => {
              return selectVal.$key === val.$key;
            });
          });

          this.allUsers = mapUsers;

          console.log('ALL USERS: ', mapUsers);
          
        });
      });
    }).catch(error => {
      this.noContactsToShow = true;
      console.log('ERROR: ', error);
    });
  }

  selected(user, status) {
    this.contactServ.selectedContact(this.uid, user.$key, status).then(() => {
      console.log('Success Update');
    });
  }

}
