import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase';

@Injectable()
export class ContactService {

  constructor(
    public auth: AngularFireAuth,
    public db: AngularFireDatabase) {
    console.debug('Hello ContactService Provider');
  }

  getAllUser() {
    return firebase.database().ref('users').once('value');
  }

  getContacts(uid) {
    return this.db.list('contacts/' + uid);
  }

  getContactsPromise(uid) {
    return firebase.database().ref('contacts').child(uid).once('value');
  }

  selectedContact(uid, user, status) {
    const data: Object = {};

    data[uid + '/' + user] = status;

    return this.db.object('contacts/').update(data);
  }

}
