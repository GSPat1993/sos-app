import { Component } from '@angular/core';
import { Contacts } from '@ionic-native/Contacts';
import { Geolocation } from '@ionic-native/geolocation';
import { NavController, Platform, LoadingController } from 'ionic-angular';

import { AppService } from '../../app/app.service'
import { ContactService } from '../contact/contact.service'
import { LocationService } from './location.service'

import {
  CameraPosition,
  GoogleMap,
  GoogleMaps,
  GoogleMapsEvent,
  HtmlInfoWindow,
  Marker,
  MarkerOptions,
  AnimateCameraOptions
} from '@ionic-native/google-maps';

import * as _ from 'lodash';

@Component({
  selector: 'page-location',
  templateUrl: 'location.html'
})
export class LocationPage {
  map: GoogleMap;
  uid: any;
  users: any;
  loader: any;
  address: any;
  allUsers: any;
  postal_code: any;
  selectedContact: any;
  notFound: Boolean; 
  noContactsToShow: Boolean;

  constructor(
                public contact: Contacts,
                public platform: Platform,
                public appServ: AppService,
                public navCtrl: NavController,
                public googleMaps: GoogleMaps,
                public geolocation: Geolocation,
                public contactServ: ContactService,
                public locationServ: LocationService,
                public loadingCtrl: LoadingController
              ) {

  }

  ionViewDidEnter() {
    let phoneContacts: Array<any>;
    let existContacts: Array<any>;
    
    this.selectedContact = null;
    this.notFound = false;
    this.uid = this.appServ.uid;
    this.loader = this.loadingCtrl.create({
      content: 'locating current position...'
    });
    // this.loader.present();

    this.contactServ.getAllUser().then(allUser => {
      phoneContacts = [];
      existContacts = [];
      this.users = allUser.val();

      if (!this.users) {
        this.noContactsToShow = true;
        return;
      }

      this.contact.find(['phoneNumbers', 'displayName'], {filter: "", hasPhoneNumber: true, desiredFields: ['phoneNumbers']}).then(phoneBook => {
        console.log('Phonebook: ', phoneBook);
        
        _.forEach(phoneBook, (phoneValue) => {
          _.forEach(phoneValue.phoneNumbers, (valPhone) => {
            phoneContacts.push(valPhone.value.replace(/\s/g, '').replace('+63', '').replace('+63', '').replace(/^0+/, ''));
            _.forOwn(this.users, (userPhone, keyPhone) => {
              if (userPhone.phone_number === (valPhone.value).replace(/\s/g, '').replace('+63', '').replace(/^0+/, '') && keyPhone !== this.uid) {
                existContacts.push(keyPhone);
              }
            });
          });
        });

        this.allUsers = _.map(_.uniq(existContacts), (val, key) => {
          return {$key: val, active: this.users[val].active, date_registered: this.users[val].date_registered, email: this.users[val].email, firstname: this.users[val].firstname, lastname: this.users[val].lastname, phone_number: this.users[val].phone_number};
        });

        console.log('ALL USERS: ', this.allUsers);
        console.log('PHONE CONTACTS: ', phoneContacts);
        console.log('EXIST CONTACTS: ', _.uniq(existContacts));
      });
    }).catch(error => {
      this.noContactsToShow = true;
      console.log('ERROR: ', error);
    });

    this.loadMap();
    // this.geolocate();
  }

  loadMap() {
    
    let element: HTMLElement = document.getElementById('map');
    this.map = this.googleMaps.create(element);

    const coords = {
      lat: this.appServ.coords.latitude,
      lng: this.appServ.coords.longitude
    };
    
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      console.log('Map is ready! ');
      let position: CameraPosition = {
        target: coords,
        zoom: 13,
        bearing: 140
      };

      this.map.moveCamera(position);
      this.map.setCameraTarget(coords);
      this.map.setMyLocationEnabled(true);

      this.loader.dismiss();
    });
  }

  selected() {
    let content: any;
    let fullName: any;
    let coordinate: any;
    // let infoWindow: HtmlInfoWindow;
    let markerOptions: MarkerOptions;
    let animateOptions: AnimateCameraOptions;

    fullName = this.selectedContact.firstname + ' ' + this.selectedContact.lastname;
    console.log('Selected: ', this.selectedContact);

    this.locationServ.getLocation(this.selectedContact.$key).then(coordSnap => {
      coordinate = coordSnap.val();
      
      if(!coordSnap.val()) {
        this.notFound = true;
      } else {
        this.notFound = false;
        
        markerOptions = {
          position: {
            lat: +coordinate.coords.latitude,
            lng: +coordinate.coords.longitude
          },
          title: fullName,
          snippet: 'Time Stamp: ' + new Date(coordinate.timestamp)
        };

        animateOptions = {
          target: {lat: +coordinate.coords.latitude, lng: +coordinate.coords.longitude},
          zoom: 17,
          tilt: 60,
          bearing: 140,
          duration: 3000
        };

        // content = 'COME ON EVERYBODY!!'; 
        // content += '<div>';
        // content +=  '<h2> ' + fullName + '</h2>';
        // content += '</div>';
        // content += '<div>';
        // content +=  '<table>';
        // content +=    '<tr><td> Phone Number: </td></tr>';
        // content +=  '<table>';
        // content += '</div>';
        this.map.clear();
        this.map.animateCamera(animateOptions).then(() => {
          this.map.addMarker(markerOptions).then((marker: Marker) => {
            marker.setAnimation('DROP');
            // infoWindow.setContent(content);
            // infoWindow.open(marker);
            console.log('MARKER');
          });
        });


        console.log('COORDS: ', coordSnap.val());
      }
      
    });
  }

  // geolocate() {
  //   if (this.platform) {
  //     this.platform.ready().then(() => {
  //       if (this.platform.is('cordova')) {
  //         this.geolocation.getCurrentPosition().then((resp) => {
  //           console.debug('resp.coords.latitude', resp.coords.latitude);
  //           console.debug('resp.coords.longitude', resp.coords.longitude);
  //           const latlng = {
  //             lat: resp.coords.latitude,
  //             lng: resp.coords.longitude
  //           };
  //           this.loadMap(latlng);
  //           this.loader.dismiss();
  //           this.reverseGeocode(latlng);
  //         }).catch((error) => {
  //           ////console.debug('Error getting location', error);
  //         });
  //       }
  //     });
  //   }
  // }

  // reverseGeocode(latlng) {
  //   let geocoder = new google.maps.Geocoder();
  //   let latlng_float = {lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng)};

  //   geocoder.geocode({'location': latlng_float}, (results: any, status: any) => {
  //     // console.log('Result: ', results);
  //     // console.log('Status: ', status);
  //     if (status === 'OK') {
  //       if (results[0]) {
  //         console.debug('REVERSE GEOCODE RESULTS: ', results);
  //         let postal_code_find = _.find(results[0].address_components, (ac: any) => {
  //           return ac.types[0] === 'postal_code';
  //         });
  //         this.address = results[0].formatted_address;
  //         postal_code_find && (this.postal_code = postal_code_find.short_name);
  //         console.log(postal_code_find);
  //       } else {
  //         window.alert('No results found');
  //       }
  //     } else {
  //       window.alert('Geocoder failed due to: ' + status);
  //     }
  //   });
  // }
}