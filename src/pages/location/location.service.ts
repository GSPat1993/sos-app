import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase';

@Injectable()
export class LocationService {

  constructor(
    public auth: AngularFireAuth,
    public db: AngularFireDatabase) {
    console.debug('Hello LocationService Provider');
  }

  getLocation(uid) {
    return firebase.database().ref('coords').child(uid).once('value');
  }

}
