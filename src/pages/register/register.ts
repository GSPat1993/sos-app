import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, ViewController, App } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
// import { Storage } from '@ionic/storage';

import { RegisterService } from './register.service';
// import { AuthService } from '../../providers/auth.service';
// import { EnterAddressPage } from '../enter-address/enter-address';

import { emailValidator, matchingPasswords } from '../../validators/validators';

import * as _ from 'lodash';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  registrationForm: FormGroup;
  loader:any;
  user_info: any;
  subscription: any;

  constructor(
    public app: App,
    public fb: FormBuilder,
    public navCtrl: NavController,
    public regServ: RegisterService,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {

    this.registrationForm = this.fb.group({
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      email: ['', Validators.compose([Validators.required, emailValidator])],
      phone_number: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10),Validators.pattern('[0-9]*')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    }, {validator: matchingPasswords('password', 'confirmPassword')});
  }

  ionViewDidLoad() {
    ////console.debug('Hello RegisterPage Page');
    // this.appServ.getDeviceInfo();
    // console.log('Platform: ', this.appServ.device_platform);
  }

  register(formData) {
    let credentials: any;

    this.loader = this.loadingCtrl.create({
      content: "submitting information..."
    });
    this.loader.present();

    formData.email = _.toLower(formData.email); 

    credentials = {
      email: formData.email,
      password: formData.password
    };

    this.regServ.register(credentials).then((user) => {
      delete formData.password;
      delete formData.confirmPassword;

      this.regServ.createUserProfile(user, formData).then(() => {
        window.sessionStorage.setItem('user_id', user.uid);
        this.loader.dismiss();
        this.viewCtrl.dismiss();
        this.showConfirm(credentials);
      }).catch((error) => {
        this.loader.dismiss();
        this.alertPopup("Profile creation failed!",error.message);
      });
    }).catch((error) => {
      this.loader.dismiss();
      this.alertPopup("ERROR!", error.message);
    });
  }

  toUnsubscribe() {
    // this.subscription.unsubscribe();
  }

  alertPopup(title,message) {
    let prompt = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: ['Ok']
    });
    prompt.present();
  }
  
  showConfirm(credentials) {
    let confirm = this.alertCtrl.create({
      title: 'Registration Success!',
      message: 'Thanks for signing up!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {      
          }
        }
      ]
    });
    confirm.present();
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  // sendNotification(notification_data, to) {
  //   this.appServ.getFCMTokens(to).then((tokens: Array<any>) => {
  //     notification_data['to'] = tokens;//_.join(tokens, ',');
  //     ////console.debug('notification_data', notification_data);
  //     this.appServ.sendNotification(notification_data).subscribe((success) => {
  //       ////console.debug('success',success);
  //     });
  //   });
  // }

}
