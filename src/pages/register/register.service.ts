import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase';

@Injectable()
export class RegisterService {

  constructor(
    public auth: AngularFireAuth,
    public db: AngularFireDatabase) {
    console.debug('Hello RegisterService Provider');
  }

  register(registerForm){
    return this.auth.auth.createUserWithEmailAndPassword(registerForm.email, registerForm.password);
  }

  createUserProfile(usr,userProfile){
    userProfile['active'] = true;
    userProfile['date_registered'] = firebase.database.ServerValue.TIMESTAMP;

    return this.db.list('/users').update(usr.uid,userProfile);
  }

}
