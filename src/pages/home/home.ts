import { SMS } from '@ionic-native/sms';
import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { NavController, Platform, LoadingController, AlertController } from 'ionic-angular';

import { AppService } from '../../app/app.service'
import { ContactService } from '../contact/contact.service';

import { LoginPage } from '../login/login';

import * as _ from 'lodash';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  uid: any;
  latlng: any;
  loader: any;
  address: any;
  postal_code: any;

  constructor(
                public sms: SMS,
                public platform: Platform,
                public appServ: AppService,
                public navCtrl: NavController,
                public geolocation: Geolocation,
                public alertCtrl: AlertController,
                public contactServ: ContactService,
                public loadingCtrl: LoadingController,
                public launchNavigator: LaunchNavigator
              ) { }

  ionViewDidEnter() {
    // this.geolocate();
    this.latlng = this.appServ.coords;
    this.uid = sessionStorage.getItem('user_id');
  }

  // geolocate() {
  //   if (this.platform) {
  //     this.platform.ready().then(() => {
  //       if (this.platform.is('cordova')) {
  //         this.geolocation.getCurrentPosition().then((resp) => {
  //           console.debug('resp.coords.latitude', resp.coords.latitude);
  //           console.debug('resp.coords.longitude', resp.coords.longitude);
  //           const latlng = {
  //             lat: resp.coords.latitude,
  //             lng: resp.coords.longitude
  //           };
  //           this.latlng = latlng;
  //           this.reverseGeocode(latlng);
  //         }).catch((error) => {
  //           ////console.debug('Error getting location', error);
  //         });
  //       }
  //     });
  //   }
  // }

  // reverseGeocode(latlng) {
  //   let geocoder = new google.maps.Geocoder();
  //   let latlng_float = {lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng)};

  //   geocoder.geocode({'location': latlng_float}, (results: any, status: any) => {
  //     if (status === 'OK') {
  //       if (results[0]) {
  //         console.debug('REVERSE GEOCODE RESULTS: ', results);
  //         let postal_code_find = _.find(results[0].address_components, (ac: any) => {
  //           return ac.types[0] === 'postal_code';
  //         });
  //         this.address = results[0].formatted_address;
  //         postal_code_find && (this.postal_code = postal_code_find.short_name);
  //         console.log(postal_code_find);
  //       } else {
  //         window.alert('No results found');
  //       }
  //     } else {
  //       window.alert('Geocoder failed due to: ' + status);
  //     }
  //   });
  // }

  sendMessage() {
    let users: any = {};
    let phoneNumbers: Array<any>;
    let alert = this.alertCtrl.create({
      title: 'No Contacts',
      subTitle: 'No contact/s was set for emergency SMS.',
      buttons: ['OK']
    });

    this.loader = this.loadingCtrl.create({
      content: 'sending message...'
    });
    this.loader.present();

    this.contactServ.getAllUser().then(allUser => {
      this.contactServ.getContactsPromise(this.uid).then(selectedContacts => {
        phoneNumbers = [];
        users = allUser.val();

        if (!selectedContacts.val()) {
          alert.present();
          this.loader.dismiss();
          return;
        }

        console.log('USERS: ', allUser.val());
        console.log('RESULT: ', selectedContacts.val());

        _.forEach(selectedContacts.val(), (selectVal, key) => {
          // phoneNumbers.push('63' + users[key].phone_number);
          phoneNumbers.push(this.sms.send('63' + users[key].phone_number, 'Help! I am in danger. Follow my location. \n\n http://maps.google.com/?q='+ this.latlng.latitude + ',' + this.latlng.longitude));
        });

        Promise.all(phoneNumbers).then((sent) => {
          console.log('SENT: ', sent);
          this.loader.dismiss();
        });

        // console.log('PHONE NUMBERS: ', phoneNumbers);

        // this.sms.send(["639085978476", "639322628869"], 'Help! I am in danger. Follow my location. \n\n http://maps.google.com/?q='+ this.latlng.lat + ',' + this.latlng.lng).then(sent => {
        //   console.log('SENT: ', sent);
        //   this.loader.dismiss();
        // }).catch(error => {
        //   console.log('ERROR SENT: ', error.message);
        // });
      });
    })
    // this.message = 'Help! I am in danger. Follow my location.';
  }

  navigateMaps() {
    console.log(this.latlng.lat + ', ' + this.latlng.lng);
    this.launchNavigator.navigate([this.latlng.lat, this.latlng.lng])
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  logout() {
    this.appServ.auth.auth.signOut().then(() => {
      localStorage.clear();
      sessionStorage.clear();
      // this.navCtrl.push(LoginPage);
    });
  }
}
