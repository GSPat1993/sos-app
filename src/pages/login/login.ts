import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Events, LoadingController, ModalController } from 'ionic-angular';
// import * as lodash from "lodash";

import { AngularFireAuth } from 'angularfire2/auth';

// import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  loader: any;
  login_form: any;

  constructor(
    public events: Events,
    public fb: FormBuilder, 
    public navCtrl: NavController,
    public afAuth: AngularFireAuth, 
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
  ) {

      this.login_form = this.initForm();
    }

  ionViewDidLoad() {
    console.debug('Hello LoginPage Page');
  }

  initForm() {
    return this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(event) {
    this.loader = this.loadingCtrl.create({
      content: 'logging in...'
    });
    this.loader.present();

    this.afAuth.auth.signInWithEmailAndPassword(this.login_form.value.email, this.login_form.value.password).then((auth) => {
      // this.storage.ready().then(() => {
        console.debug('this.login_form.value.email',this.login_form.value.email);
      // });

      this.onSignInSuccess(auth);
    })
    .catch((error) => {
      alert(error.message);
      this.onSignInError();
    });
  }

  onSignInSuccess(auth): void {
    console.debug("success logged in",auth);
    // this.appServ.getData(auth).then(() => {
      this.loader.dismiss();
      // this.navCtrl.push(HomePage);
    // }).catch((error) => {
    //   this.loader.dismiss();
    // });
  }

  onSignInError(): void {    
    console.debug("error logged in");
    this.loader.dismiss();
  }

  register() {
    this.modalCtrl.create(RegisterPage).present();
  }
}
