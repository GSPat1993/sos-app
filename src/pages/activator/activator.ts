import { SMS } from '@ionic-native/sms';
import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { NavController, Platform, LoadingController, AlertController } from 'ionic-angular';

import { AppService } from '../../app/app.service'
import { ContactService } from '../contact/contact.service';

// import { LoginPage } from '../login/login';

import * as _ from 'lodash';

@Component({
  selector: 'page-activator',
  templateUrl: 'activator.html'
})
export class ActivatorPage {

  constructor(
                public sms: SMS,
                public platform: Platform,
                public appServ: AppService,
                public navCtrl: NavController,
                public geolocation: Geolocation,
                public alertCtrl: AlertController,
                public contactServ: ContactService,
                public loadingCtrl: LoadingController,
                public launchNavigator: LaunchNavigator
              ) { }
}
