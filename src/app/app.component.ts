import { AppService } from './app.service';
import { Platform, Nav } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ActivatorPage } from '../pages/activator/activator';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';

import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  rootPage:any;
  uid: any;

  constructor(
      public platform: Platform,
      public appServ: AppService,
      public statusBar: StatusBar,
      public afAuth: AngularFireAuth,
      public geolocation: Geolocation,
      public splashScreen: SplashScreen
    ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ngOnInit() {
    this.appServ.checkActive().subscribe(isActive => {
      console.log('IS ACTIVE: ', isActive[0].$value);

      if (isActive[0].$value) {
        this.afAuth.authState.subscribe(auth => {
          if (auth) {
            console.debug('authenticated: ',auth);
            window.sessionStorage.setItem('user_id',auth.uid);
            window.sessionStorage.setItem('auth', JSON.stringify(auth));
            this.appServ.uid = auth.uid;
            this.uid = auth.uid;
    
            this.nav.setRoot(TabsPage);
            this.watchCurrentPosition();
          } else {
            this.nav.setRoot(LoginPage);
          }
        });
      } else {
        this.nav.setRoot(ActivatorPage);
      }
    });

  }

  watchCurrentPosition() {
    this.geolocation.watchPosition().subscribe(position => {
      this.appServ.setLatLong(this.uid, position.coords).then(success => {
        console.log('POSITION: ', position);
        console.log(position.coords.longitude + ' ' + position.coords.latitude);
         
        this.appServ.coords = position.coords;
      });
    });
  }
}
