import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase';

@Injectable()
export class AppService {

  coords: any;
  uid: any;

  constructor(
    public auth: AngularFireAuth,
    public db: AngularFireDatabase) {
    console.debug('Hello AppService Provider');
  }

  setLatLong(uid, coords) {
    return firebase.database().ref('coords').child(uid).set({
      coords: {
        latitude: coords.latitude,
        longitude: coords.longitude
      },
      timestamp: firebase.database.ServerValue.TIMESTAMP
    });
  }

  updateLatLong(uid) {
    return new Promise((resolve, reject) => {
      firebase.database().ref('coords').child(uid).on('child_changed', (snap) => {
        const position = snap.val();

        resolve(position);
      });
    });
  }

  checkActive() {
    return this.db.list('activator');
  }
}
