import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';
import { AppService } from './app.service';

import { ActivatorPage } from '../pages/activator/activator';
import { LocationPage } from '../pages/location/location';
import { RegisterPage } from '../pages/register/register';
import { ContactPage } from '../pages/contact/contact';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

//Import Providers
import { LocationService } from '../pages/location/location.service';
import { RegisterService } from '../pages/register/register.service';
import { ContactService } from '../pages/contact/contact.service';
import { LoginService } from '../pages/login/login.service';

import { SMS } from '@ionic-native/sms';
import { Contacts } from '@ionic-native/Contacts';
import { StatusBar } from '@ionic-native/status-bar';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

export const firebaseConfig = {
    apiKey: "AIzaSyBTIiKbsbw7NdSgiea8DkKROtTX815GmqQ",
    authDomain: "sos-app-bad86.firebaseapp.com",
    databaseURL: "https://sos-app-bad86.firebaseio.com",
    projectId: "sos-app-bad86",
    storageBucket: "sos-app-bad86.appspot.com",
    messagingSenderId: "897419116489"
};

@NgModule({
  declarations: [
    MyApp,
    ActivatorPage,
    LocationPage,
    RegisterPage,
    ContactPage,
    LoginPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ActivatorPage,
    LocationPage,
    RegisterPage,
    ContactPage,
    LoginPage,
    HomePage,
    TabsPage
  ],
  providers: [
    SMS,
    Contacts,
    StatusBar,
    GoogleMaps,
    AppService,
    Geolocation,
    SplashScreen,
    LoginService,
    ContactService,
    RegisterService,
    LocationService,
    LaunchNavigator,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
